#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import torch
from spring_msgs.msg import RawAudioData
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from audio_processing_model import mode_eval_pub
import numpy as np

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")
FILTER_SIZE = rospy.get_param('/audio_processing/filter_size_param')

class AudioProcessing(object):
    def __init__(self, name):
        self.model = mode_eval_pub.load_model()
        self.mode = mode_eval_pub.GetFrame()
        rospy.loginfo("Starting {}.".format(name))
        self.pub = rospy.Publisher("audio/enh_audio", RawAudioData,queue_size=1)
        self.diagnostics_frame=[0]
        self.ari_speech_counter = 0
        self.__is_alive()

    def __is_alive(self):
        from threading import Thread
        from std_msgs.msg import String

        pub = rospy.Publisher("~is_alive", String, queue_size=1)
        diagnostics_pub =rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
        diagnostocs_arr = DiagnosticArray()
        def publish():
            r = rospy.Rate(1)
            while not rospy.is_shutdown():
                if len(np.unique(self.diagnostics_frame))>0:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.OK,name='Audio: Audio preprocessing: MoDe',message='Speech enhancement Running OK')
                else:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.WARN,name='Audio: Audio preprocessing: MoDe',message='Speech enhancement Not publishing well')
                pub.publish(str(rospy.Time.now().to_sec()))
                diagnostocs_arr.status = [msg1]
                diagnostocs_arr.header.stamp=rospy.Time.now()
                diagnostics_pub.publish(diagnostocs_arr)
                r.sleep()

        t = Thread(target=publish)
        t.start()

    def sub(self):
        rospy.Subscriber("/audio/raw_audio", RawAudioData, self.callback, queue_size=10)
    def callback(self, audio):
        new_frame=np.array([]).astype(np.int16)
        msg = RawAudioData()
        s = np.array(audio.data).reshape(512,6)[:,5]
        if len(np.unique(s)) > 1:
            self.ari_speech_counter=FILTER_SIZE
        else:
            self.ari_speech_counter=max(0,self.ari_speech_counter-1)
        ari_speech_idx =np.where(s!=0)
        
        msg.header.stamp = audio.header.stamp
        msg.nb_channel = 1
        msg.rate = audio.rate
        msg.format = audio.format
        msg.sample_byte_size = audio.sample_byte_size
        msg.nb_frames = audio.nb_frames
        nsy_ad=audio.data
        nsy_data = np.array(nsy_ad).reshape(512,6)
        raw_frame = nsy_data[:,1].reshape(4,128)
        for delta in range(4):
            get_output=self.mode.handle_delta((raw_frame[delta,:].astype(np.float32)/2**15),self.model)
            if get_output is not None:
                new_frame=np.append(new_frame, get_output)
        if len(new_frame) !=0:
            if len(ari_speech_idx[0])>1 or self.ari_speech_counter!=0:
                msg.header.frame_id = 'ARI'
                new_frame=list(new_frame*0)
            else: 
                new_frame=list(new_frame)
            self.diagnostics_frame = new_frame
        msg.data = new_frame
        self.pub.publish(msg) #published enh_data to audio/enh_audio topic

if __name__ == "__main__":
    rospy.init_node("audio_processing")
    a = AudioProcessing("audio_processing")
    a.sub()
    rospy.spin()
