from re import S
import numpy as np
import torch
from torch._C import device
import torch.nn as nn
from scipy.io import loadmat
from torch.nn import functional as F
import rospy

#import os
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "2"  # specify which GPU(s) to be used

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")
rospy.loginfo(use_cuda,device)

if use_cuda:torch.set_default_tensor_type(torch.cuda.FloatTensor)
else:
    torch.set_default_tensor_type(torch.FloatTensor)

""" Model """
class Expert(nn.Module):
    def __init__(self, output_size=257):
        super().__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=3, padding=2),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        self.flatten = nn.Flatten()
        self.fc1 = nn.Sequential(
            nn.Linear(16640, 500),
            nn.BatchNorm1d(500),
            nn.ReLU(inplace=True)
        )
        self.fc2 = nn.Sequential(
            nn.Linear(500, output_size),
            nn.BatchNorm1d(output_size),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.fc1(self.flatten(x))
        x = self.fc2(x)
        return x


class Gate(nn.Module):
    def __init__(self, num_experts):
        super().__init__()
        self.fc1 = nn.Sequential(
            nn.Linear(2570, 512),
            nn.BatchNorm1d(512),
            nn.ReLU()
        )
        self.fc2 = nn.Sequential(
            nn.Linear(512, num_experts),
            nn.BatchNorm1d(num_experts),
            nn.Softmax(dim=-1)
        )

    def forward(self, x):
        x = self.fc1(x)
        x = self.fc2(x)
        return x


class MODE(nn.Module):
    def __init__(self, num_experts=3, output_size=257, context=10):
        super().__init__()
        self.experts = nn.ModuleList([Expert(output_size) for _ in range(num_experts)])
        self.gate = Gate(num_experts)
        self.num_experts = num_experts
        self.output_size = output_size
        self.context = context
    def forward(self, x):
        experts_out = torch.stack([expert(x) for expert in self.experts], dim=1)
        gate_out = self.gate(x.view(-1, self.output_size * self.context))
        out = experts_out * gate_out.unsqueeze(-1)
        out = out.sum(1)
        return out, (experts_out, gate_out)


class GetFrame(object):
    def __init__(self):
        self.current_data_index = 0
        self.frame_index = 0
        self.beta_pass = torch.zeros((257, 1)).to(device)
        self.beta_pass[0:85] = float(rospy.get_param("~beta_high"))
        self.beta_pass[86:257] = float(rospy.get_param("~beta_low"))
        self.tensor_frame = torch.zeros((0,), device=torch.device(device))  # one frame
        self.buffer = torch.zeros((512,), device=torch.device(device))
        self.buffer_zeros = torch.zeros((128,), device=torch.device(device))
        self.synt = torch.tensor(loadmat(rospy.get_param("~get_frame_synt_win_param"))['synt_win'], device=torch.device(device))
        self.params = {
            # signal params
            'frame_rate': 16000,
            'sample_width': 2,
            'channels': 1,
            # spp params
            'spp_threshold': 10,  # 10 - no infuance on VAD; 100 - VAD has influnce
            'spp_vad_attenuation_factor': 0.5,  # 0.03 - default value ;
            'beta': self.beta_pass,
            # stft params
            'nfft': 512,
            'overlap': 0.75,
            'eps': 1e-6,
            'synt_win': self.synt,
        }
        self.stft_args = {'nfft': 512, 'win_length': 512,
                          'win_step': int(512 * (1 - 0.75)),
                          'window': torch.hann_window(512)}


    @torch.no_grad()
    def handle_delta(self, data,model):
        # first scenario:
        if self.current_data_index < 13:
            self.tensor_frame = torch.cat((self.tensor_frame, torch.tensor(data).to(device)), dim=0).to(device)
            self.current_data_index += 1

        if self.current_data_index == 13:
            noisy_full_stft = stft(self.tensor_frame, return_complex=True, **self.stft_args).squeeze()
            tensor_frame_transform = torch.log10(noisy_full_stft.abs() + 1e-10).to(device)
            tensor_frame_transform = tensor_frame_transform.squeeze()
            tensor_frame_transform = tensor_frame_transform[None, None, :]
            tensor_frame_transform = torch.transpose(tensor_frame_transform, 3, 2)
            out, _ = model(tensor_frame_transform.contiguous())
            
            # enhance
            enhance_sig = enhance_signal(noisy_full_stft.detach(),
                                         estimate_irm=out.detach(),
                                         synt_win=self.synt,
                                         vad_thr=self.params['spp_threshold'],
                                         vad_attenuation=self.params['spp_vad_attenuation_factor'],
                                         beta=self.params['beta'], **self.stft_args)

            enhance_sig = enhance_sig.squeeze()
            self.buffer = self.buffer + enhance_sig
            delta_clean = torch.clone(self.buffer[:128])
            self.buffer = torch.clone(self.buffer[128:])
            self.buffer = torch.cat((self.buffer, self.buffer_zeros), dim=0)
            output=np.array(delta_clean.cpu().numpy().flatten()*2**15).astype(np.int16)
            
            self.tensor_frame = torch.clone(self.tensor_frame[128:])
            self.current_data_index -= 1
            return output



""" Data pipeline """


def stft(input, nfft, win_length, win_step, window=None, return_complex=False):
    """
    The STFT computes the Fourier transform of short overlapping windows of the input.
    This giving frequency components of the signal as they change over time.

    Args:
        input (Tensor): 1D time sequence signal tensor.
        nfft (int): size of Fourier transform.
        win_length (int): the size of window frame and STFT filter.
        win_step (int): the distance between neighboring sliding window frames.
        window (Tensor, optional): the optional window function.
        return_complex (bool, default=False): Whether to return magnitude or complex tensor.

    Returns:
        A tensor containing the STFT result (complex or magnitude).
    """
    from torch.fft import rfft

    wsig = input.unfold(0, win_length, win_step).to(device)
    if window is not None:
        wsig = torch.mul(wsig, window.to(device))

    padded_wsig = F.pad(wsig, [0, nfft - win_length], mode='constant', value=0)
    padded_wsig = padded_wsig.view(padded_wsig.shape[0], 1, padded_wsig.shape[1])
    fft_complex = rfft(padded_wsig)
    if return_complex:
        return fft_complex
    return fft_complex.abs()


""" Enhancement """


@torch.no_grad()
def enhance_signal(input, estimate_irm, synt_win, do_vad=True, vad_thr=10, vad_attenuation=0.03, beta=None,
                   **stft_args):
    mask = estimate_irm.T
    stft_for_enhance = input.T

    if do_vad:
        vad = torch.abs(mask.sum(axis=0)).to(device) < vad_thr
    else:
        vad = torch.zeros(mask.shape[1], dtype=np.bool).to(device)
    mask[0:2, :] = 0
    mask[:, vad == 1] = mask[:, vad == 1] * vad_attenuation

    est_s = enhancement(mask, stft_for_enhance, synt_win=synt_win, beta=beta, **stft_args)

    return est_s

def enhancement(irm, noisy_stft, nfft, win_length, synt_win, beta, **kwargs):
    # irm [257, 1]
    # noisy_stft  [257, 1]
    # synt_win [512]

    stft_size = nfft // 2 + 1

    seg = 1

    stft_abs = torch.abs(noisy_stft[:, seg - 1]).reshape((stft_size, 1)).to(device)
    stft_phase = torch.angle(noisy_stft[:, seg - 1]).reshape((stft_size, 1)).to(device)

    a_hat = stft_abs * (beta ** (1 - irm))  # adaptation of eq (3)

    a_hat[0:3] = a_hat[0:3] * 0.001

    a_hat_inv = a_hat.flip((0,))
    a_hat_inv = a_hat_inv[1:stft_size - 1]
    a_hat_full = torch.cat((a_hat, a_hat_inv), dim=0).reshape((win_length, 1)).to(device)

    p_inv = stft_phase.flip((0,))
    p_inv = p_inv[1:stft_size - 1]
    p_full = torch.cat((stft_phase, -p_inv)).reshape((win_length, 1)).to(device)

    estimated_stft = a_hat_full * torch.exp(1j * p_full).to(device)
    r1 = torch.real(torch.fft.ifft(estimated_stft.T)).to(device)

    s_est = (r1.T * synt_win).to(device)

    return s_est


def load_model():
    model = MODE(num_experts=3, output_size=512 // 2 + 1, context=10)
    loaded = torch.load(rospy.get_param("~model_load_state_dict"), map_location=device)
    model.load_state_dict(loaded['state_dict'])
    model.to(device)
    model.eval()
    return model
