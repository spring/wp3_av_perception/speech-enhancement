## Audio enhancement ROS package</tt>
This package should run on a docker container on a pc outside ARI.
The package supports GPU computation with CUDA.

After downloading the package to `catkin_ws/src`, build the package with `catkin_make`.

To launch the package:

`export ROS_MASTER_URI=http://ari-Xc:11311`

`export ROS_IP=XXX.YYY.ZZZ.WWW #your PC's IP`

`roslaunch mode_eval_launch mode_eval.launch`


### A Docker container that runs the app is on this GitLab Docker repository
